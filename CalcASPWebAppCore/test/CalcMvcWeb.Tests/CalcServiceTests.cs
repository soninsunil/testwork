﻿using CalcMvcWeb.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CalcMvcWeb.Tests
{
    public class CalcServiceTests
    {
        [Fact]
        public void TestAddNumbers()
        {
            // 1. Arrange
            var cs = new CalcService();

            // 2. Act 
            var result = cs.AddNumbers(5, 5);

            // 3. Assert 
            Assert.Equal(10, result);
        }
        [Fact]
        public void TestSubtractNumbers()
        {
            var cs = new CalcService();
            var result = cs.SubtractNumbers(10, 2);
            Assert.Equal(8, result);
        }
        [Fact]
        public void TestMultiplyNumbers()
        {
            var cs = new CalcService();
            var actualResult = cs.MultiplyNumbers(15, 3);
            Assert.Equal(45, actualResult);
        }
        [Fact]
        public void TestEvenNumberWithBoolCompare()
        {
            var cs = new CalcService();
            var actualResult = cs.IsEven(8);
            Assert.True(actualResult);
        }
        [Fact]
        public void TestEvenNumberForTrueResult()
        {
            var cs = new CalcService();
            var actualResult = cs.IsEven(3);
            Assert.False(actualResult);
        }
        [Fact]
        public void TestEvenOrOdd()
        {
            var cs = new CalcService();
            var actualResult = cs.IsEvenOrOdd(4);
            Assert.True(actualResult);
        }
        [Fact]
        public void TestDivideByZero()
        {
            var cs = new CalcService();
            //var actualResult = cs.UnsafeDivide(x, y);
            
            Exception ex = Assert
                .Throws<DivideByZeroException>(() => cs.UnsafeDivide(1, 0));
        }
    }
}
